module bitbucket.org/uwaploe/tsfpga

go 1.15

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/urfave/cli v1.22.5
	google.golang.org/grpc v1.36.1
	google.golang.org/protobuf v1.26.0
	periph.io/x/conn/v3 v3.7.0
)
