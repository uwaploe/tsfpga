// Tsfpgasvc provides a gRPC server to access the FPGA memory space on a
// Technologic Systems TS-4200 or TS-4800 CPU board.
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"

	"bitbucket.org/uwaploe/tsfpga/api"
	"bitbucket.org/uwaploe/tsfpga/internal/server"
	"bitbucket.org/uwaploe/tsfpga/internal/tsmem"
	"google.golang.org/grpc"
)

const Usage = `Usage: tsfpgasvc [options] ts4200|ts4800

Tsfpgasvc provides a gRPC server to access the FPGA memory space on a
Technologic Systems TS-4200 or TS-4800 CPU board.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	svcAddr string = "0.0.0.0:10101"
)

const muxbusCfg uint16 = 0x181

var boardCfg = map[string]struct {
	base, size uint32
	muxbusReg  uint32
	dio        map[string]server.Dio
}{
	"ts4200": {
		base:      0x30000000,
		size:      0x800,
		muxbusReg: 0x20,
		dio: map[string]server.Dio{
			"8160_LCD_D0": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x01},
			"8160_LCD_D1": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x02},
			"8160_LCD_D2": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x04},
			"8160_LCD_D3": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x08},
			"8160_LCD_D4": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x10},
			"8160_LCD_D5": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x20},
			"8160_LCD_D6": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x40},
			"8160_LCD_D7": {DirReg: 0x40a, OutReg: 0x406, InReg: 0x40e, Mask: 0x80},
			"8160_DIO_1":  {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x01},
			"8160_DIO_3":  {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x02},
			"8160_DIO_5":  {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x04},
			"8160_DIO_7":  {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x08},
			"8160_DIO_9":  {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x10},
			"8160_DIO_11": {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x20},
			"8160_DIO_13": {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x40},
			"8160_DIO_15": {DirReg: 0x408, OutReg: 0x404, InReg: 0x40c, Mask: 0x80},
		},
	},
	"ts4800": {
		base:      0xb0010000,
		size:      0x8000,
		muxbusReg: 0x12,
		dio: map[string]server.Dio{
			"8100_LCD_D0": {DirReg: 0x100a, OutReg: 0x1006, InReg: 0x100e, Mask: 0x01},
			"8100_LCD_D1": {DirReg: 0x100a, OutReg: 0x1006, InReg: 0x100e, Mask: 0x02},
			"130":         {DirReg: 0x1008, OutReg: 0x1004, InReg: 0x100c, Mask: 0x01},
			"131":         {DirReg: 0x1008, OutReg: 0x1004, InReg: 0x100c, Mask: 0x02},
			"132":         {DirReg: 0x1008, OutReg: 0x1004, InReg: 0x100c, Mask: 0x04},
			"133":         {DirReg: 0x1008, OutReg: 0x1004, InReg: 0x100c, Mask: 0x08},
		},
	},
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	cfg, ok := boardCfg[strings.ToLower(args[0])]
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown CPU board name: %q", args[0])
		os.Exit(2)
	}

	mem, err := tsmem.New(cfg.base, cfg.size)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Mmap failed: %v", err)
		os.Exit(3)
	}

	addr, _ := net.ResolveTCPAddr("tcp", svcAddr)
	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}

	// Configure the MUXBUS
	mem.WriteWord(cfg.muxbusReg, muxbusCfg)

	svc := server.New(cfg.dio, mem)
	opts := make([]grpc.ServerOption, 0)
	grpcServer := grpc.NewServer(opts...)
	api.RegisterFpgaMemServer(grpcServer, svc)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s := <-sigs
		log.Printf("Got signal: %v", s)
		grpcServer.GracefulStop()
	}()

	log.Printf("FpgaMem gRPC Server %s starting", Version)
	if err := grpcServer.Serve(listener); err != nil {
		log.Printf("Error: %v", err)
	}
}
