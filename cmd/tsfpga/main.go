// Tsfpga implements a gRPC client that provides access to the
// FPGA memory space on a TS-4200 or TS-4800 board.
package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaploe/tsfpga/api"
	pb "bitbucket.org/uwaploe/tsfpga/api"
	"github.com/urfave/cli"
	"google.golang.org/grpc"
)

var Version = "dev"
var BuildDate = "unknown"

func bitsToSize(bits int) pb.Size {
	switch bits {
	case 32:
		return pb.Size_LONG
	case 16:
		return pb.Size_WORD
	}
	return pb.Size_BYTE
}

func peek(client pb.FpgaMemClient, addr uint32, bits int) (uint32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.AddrMsg{Address: addr, Size: bitsToSize(bits)}
	val, err := client.Peek(ctx, msg)
	return val.Value, err
}

func poke(client pb.FpgaMemClient, addr uint32, bits int,
	value uint32) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.ValueMsg{
		Address: addr,
		Size:    bitsToSize(bits),
		Value:   value}
	_, err := client.Poke(ctx, msg)
	return err
}

func modify(client pb.FpgaMemClient, addr uint32, bits int,
	value, mask uint32) (uint32, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.UpdateMsg{
		Address: addr,
		Size:    bitsToSize(bits),
		Value:   value,
		Mask:    mask}
	resp, err := client.Modify(ctx, msg)
	return resp.Value, err
}

func dioset(client pb.FpgaMemClient, name string, state pb.DioState) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{
		Name:  name,
		State: state}
	_, err := client.DioSet(ctx, msg)
	return err
}

func dioget(client pb.FpgaMemClient, name string) (pb.DioState, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{Name: name}
	resp, err := client.DioGet(ctx, msg)
	if err != nil {
		return pb.DioState_LOW, err
	}
	return resp.State, nil
}

type dutyCycleFunc func(int, int) float64

func rampVal(i, n int) float64 {
	if i >= n {
		return 1
	}

	return float64(i) / float64(n)
}

func makeConstFunc(val uint) dutyCycleFunc {
	return func(i, n int) float64 {
		return float64(val) / 100
	}
}

type pwmConfig struct {
	pulseLen   time.Duration
	repCount   int
	fDutyCycle dutyCycleFunc
}

func (cfg pwmConfig) HighTime(i, n int) time.Duration {
	return time.Duration(float64(cfg.pulseLen) * cfg.fDutyCycle(i, n))
}

func (cfg pwmConfig) LowTime(i, n int) time.Duration {
	return time.Duration(float64(cfg.pulseLen) * (1.0 - cfg.fDutyCycle(i, n)))
}

func pwm(client pb.FpgaMemClient, name string, cfg pwmConfig) error {
	high := pb.DioMsg{Name: name, State: api.DioState_HIGH}
	low := pb.DioMsg{Name: name, State: api.DioState_LOW}

	stream, err := client.SetMulti(context.Background())
	if err != nil {
		return err
	}

	for i := 0; i <= cfg.repCount; i++ {
		tHigh := cfg.HighTime(i, cfg.repCount)
		tLow := cfg.LowTime(i, cfg.repCount)
		if tHigh > 0 {
			stream.Send(&high)
			time.Sleep(tHigh)
		}
		if tLow > 0 {
			stream.Send(&low)
			time.Sleep(tLow)
		}
	}
	_, err = stream.CloseAndRecv()

	return err
}

func main() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	app := cli.NewApp()
	app.Name = "tsfpga"
	app.Usage = "access the FPGA memory on a TS CPU board"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "server, s",
			Usage:  "Server address, host:port",
			Value:  "127.0.0.1:10101",
			EnvVar: "TSFPGA_HOST",
		},
		cli.VersionFlag,
	}

	var conn *grpc.ClientConn
	var client pb.FpgaMemClient

	app.Before = func(c *cli.Context) error {
		var err error
		conn, err = grpc.Dial(c.String("server"), opts...)
		if err == nil {
			client = pb.NewFpgaMemClient(conn)
		}
		return err
	}

	app.After = func(c *cli.Context) error {
		if conn != nil {
			conn.Close()
		}
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:            "peek",
			Usage:           "read a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bits",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return cli.NewExitError("missing arguments", 1)
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				bits, err := strconv.ParseInt(c.Args().Get(1), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				value, err := peek(client, uint32(addr), int(bits))
				if err != nil {
					return cli.NewExitError(err, 3)
				}
				fmt.Fprintf(c.App.Writer, "0x%08x\n", value)
				return nil
			},
		},
		{
			Name:            "poke",
			Usage:           "write to a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bits value",
			Action: func(c *cli.Context) error {
				if c.NArg() < 3 {
					return cli.NewExitError("missing arguments", 1)
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				bits, err := strconv.ParseInt(c.Args().Get(1), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				value, err := strconv.ParseInt(c.Args().Get(2), 0, 0)
				if err != nil {
					return cli.NewExitError(err, 2)
				}
				err = poke(client, uint32(addr), int(bits), uint32(value))
				if err != nil {
					return cli.NewExitError(err, 3)
				}
				return nil
			},
		},
		{
			Name:            "bitset16",
			Usage:           "set one or more bits of a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bit [bit ...]",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return cli.NewExitError("missing arguments", 1)
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				mask := uint32(0)
				for i := 1; i < c.NArg(); i++ {
					bit, err := strconv.ParseInt(c.Args().Get(i), 0, 0)
					if err != nil {
						return cli.NewExitError(err, 2)
					}
					mask = mask | (uint32(1) << uint32(bit))
				}
				_, err = modify(client, uint32(addr), 16,
					uint32(mask)&0xffff, uint32(mask)&0xffff)
				if err != nil {
					return cli.NewExitError(err, 3)
				}
				return nil
			},
		},
		{
			Name:            "bitclear16",
			Usage:           "clear one or more bits of a memory location",
			SkipFlagParsing: true,
			ArgsUsage:       "addr bit [bit ...]",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return cli.NewExitError("missing arguments", 1)
				}
				addr, err := strconv.ParseInt(c.Args().Get(0), 0, 0)
				mask := uint32(0)
				for i := 1; i < c.NArg(); i++ {
					bit, err := strconv.ParseInt(c.Args().Get(i), 0, 0)
					if err != nil {
						return cli.NewExitError(err, 2)
					}
					mask = mask | (uint32(1) << uint32(bit))
				}
				_, err = modify(client, uint32(addr), 16,
					uint32(0), uint32(mask)&0xffff)
				if err != nil {
					return cli.NewExitError(err, 3)
				}
				return nil
			},
		},
		{
			Name:            "dioset",
			Usage:           "change the state of a DIO line",
			SkipFlagParsing: true,
			ArgsUsage:       "name state",
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return cli.NewExitError("missing arguments", 1)
				}
				state, ok := pb.DioState_value[strings.ToUpper(c.Args().Get(1))]
				if !ok {
					return cli.NewExitError(fmt.Errorf("Invalid DIO state: %q", c.Args().Get(1)), 2)
				}
				err := dioset(client, strings.ToUpper(c.Args().Get(0)),
					pb.DioState(state))
				if err != nil {
					return cli.NewExitError(err, 3)
				}
				return nil
			},
		},
		{
			Name:            "dioget",
			Usage:           "read the state of a DIO line",
			SkipFlagParsing: true,
			ArgsUsage:       "name",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				state, err := dioget(client, strings.ToUpper(c.Args().Get(0)))
				if err == nil {
					fmt.Fprintf(c.App.Writer, "%s=%s\n", c.Args().Get(0),
						pb.DioState_name[int32(state)])
					return nil
				}

				return cli.NewExitError(err, 3)
			},
		},
		{
			Name:      "pwm",
			Usage:     "generate a series of PWM pulses",
			ArgsUsage: "name pulse_width duration",
			Flags: []cli.Flag{
				cli.UintFlag{
					Name:  "duty",
					Usage: "duty cycle in %",
				},
			},
			Action: func(c *cli.Context) error {
				if c.NArg() < 3 {
					return cli.NewExitError("missing arguments", 1)
				}
				pw, err := time.ParseDuration(c.Args().Get(1))
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Bad pulse width: %q",
						c.Args().Get(1)), 2)
				}
				duration, err := time.ParseDuration(c.Args().Get(2))
				if err != nil {
					return cli.NewExitError(fmt.Errorf("Bad duration: %q",
						c.Args().Get(2)), 2)
				}
				cfg := pwmConfig{pulseLen: pw, repCount: int(duration) / int(pw)}
				if duty := c.Uint("duty"); duty == 0 {
					cfg.fDutyCycle = rampVal
				} else {
					cfg.fDutyCycle = makeConstFunc(duty)
				}

				err = pwm(client,
					strings.ToUpper(c.Args().Get(0)),
					cfg)
				if err != nil {
					return cli.NewExitError(err, 3)
				}

				return nil
			},
		},
	}

	app.Run(os.Args)
}
