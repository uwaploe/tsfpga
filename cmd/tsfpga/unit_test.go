package main

import (
	"testing"
	"time"
)

func TestPwmConfig(t *testing.T) {
	fdc := makeConstFunc(50)
	tests := []struct {
		cfg     pwmConfig
		hi, low time.Duration
	}{
		{
			cfg: pwmConfig{pulseLen: time.Millisecond * 30, fDutyCycle: fdc},
			hi:  time.Millisecond * 15,
			low: time.Millisecond * 15,
		},
		{
			cfg: pwmConfig{pulseLen: time.Millisecond * 40, fDutyCycle: makeConstFunc(25)},
			hi:  time.Millisecond * 10,
			low: time.Millisecond * 30,
		},
		{
			cfg: pwmConfig{pulseLen: time.Millisecond * 40, fDutyCycle: makeConstFunc(75)},
			hi:  time.Millisecond * 30,
			low: time.Millisecond * 10,
		},
	}

	for _, test := range tests {
		hi := test.cfg.HighTime(0, 1)
		low := test.cfg.LowTime(0, 1)
		if hi != test.hi {
			t.Errorf("Bad value; expected %v, got %v", test.hi, hi)
		}
		if low != test.low {
			t.Errorf("Bad value; expected %v, got %v", test.low, low)
		}
	}
}

func TestPwmRamp(t *testing.T) {
	cfg := pwmConfig{
		pulseLen:   time.Millisecond * 10,
		repCount:   100,
		fDutyCycle: rampVal,
	}

	tests := []struct {
		cfg pwmConfig
		i   int
		hi  time.Duration
	}{
		{cfg: cfg, i: 50, hi: time.Millisecond * 5},
		{cfg: cfg, i: 25, hi: time.Microsecond * 2500},
		{cfg: cfg, i: 100, hi: time.Millisecond * 10},
		{cfg: cfg, i: 110, hi: time.Millisecond * 10},
	}

	for _, test := range tests {
		hi := test.cfg.HighTime(test.i, test.cfg.repCount)
		if hi != test.hi {
			t.Errorf("Bad value; expected %v, got %v", test.hi, hi)
		}
	}
}
