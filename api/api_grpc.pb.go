// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.4
// source: api.proto

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// FpgaMemClient is the client API for FpgaMem service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type FpgaMemClient interface {
	Peek(ctx context.Context, in *AddrMsg, opts ...grpc.CallOption) (*ValueMsg, error)
	Poke(ctx context.Context, in *ValueMsg, opts ...grpc.CallOption) (*ValueMsg, error)
	Modify(ctx context.Context, in *UpdateMsg, opts ...grpc.CallOption) (*ValueMsg, error)
	DioGet(ctx context.Context, in *DioMsg, opts ...grpc.CallOption) (*DioMsg, error)
	DioSet(ctx context.Context, in *DioMsg, opts ...grpc.CallOption) (*DioMsg, error)
	PeekMulti(ctx context.Context, opts ...grpc.CallOption) (FpgaMem_PeekMultiClient, error)
	SetMulti(ctx context.Context, opts ...grpc.CallOption) (FpgaMem_SetMultiClient, error)
}

type fpgaMemClient struct {
	cc grpc.ClientConnInterface
}

func NewFpgaMemClient(cc grpc.ClientConnInterface) FpgaMemClient {
	return &fpgaMemClient{cc}
}

func (c *fpgaMemClient) Peek(ctx context.Context, in *AddrMsg, opts ...grpc.CallOption) (*ValueMsg, error) {
	out := new(ValueMsg)
	err := c.cc.Invoke(ctx, "/api.FpgaMem/Peek", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fpgaMemClient) Poke(ctx context.Context, in *ValueMsg, opts ...grpc.CallOption) (*ValueMsg, error) {
	out := new(ValueMsg)
	err := c.cc.Invoke(ctx, "/api.FpgaMem/Poke", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fpgaMemClient) Modify(ctx context.Context, in *UpdateMsg, opts ...grpc.CallOption) (*ValueMsg, error) {
	out := new(ValueMsg)
	err := c.cc.Invoke(ctx, "/api.FpgaMem/Modify", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fpgaMemClient) DioGet(ctx context.Context, in *DioMsg, opts ...grpc.CallOption) (*DioMsg, error) {
	out := new(DioMsg)
	err := c.cc.Invoke(ctx, "/api.FpgaMem/DioGet", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fpgaMemClient) DioSet(ctx context.Context, in *DioMsg, opts ...grpc.CallOption) (*DioMsg, error) {
	out := new(DioMsg)
	err := c.cc.Invoke(ctx, "/api.FpgaMem/DioSet", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fpgaMemClient) PeekMulti(ctx context.Context, opts ...grpc.CallOption) (FpgaMem_PeekMultiClient, error) {
	stream, err := c.cc.NewStream(ctx, &FpgaMem_ServiceDesc.Streams[0], "/api.FpgaMem/PeekMulti", opts...)
	if err != nil {
		return nil, err
	}
	x := &fpgaMemPeekMultiClient{stream}
	return x, nil
}

type FpgaMem_PeekMultiClient interface {
	Send(*AddrMsg) error
	Recv() (*ValueMsg, error)
	grpc.ClientStream
}

type fpgaMemPeekMultiClient struct {
	grpc.ClientStream
}

func (x *fpgaMemPeekMultiClient) Send(m *AddrMsg) error {
	return x.ClientStream.SendMsg(m)
}

func (x *fpgaMemPeekMultiClient) Recv() (*ValueMsg, error) {
	m := new(ValueMsg)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *fpgaMemClient) SetMulti(ctx context.Context, opts ...grpc.CallOption) (FpgaMem_SetMultiClient, error) {
	stream, err := c.cc.NewStream(ctx, &FpgaMem_ServiceDesc.Streams[1], "/api.FpgaMem/SetMulti", opts...)
	if err != nil {
		return nil, err
	}
	x := &fpgaMemSetMultiClient{stream}
	return x, nil
}

type FpgaMem_SetMultiClient interface {
	Send(*DioMsg) error
	CloseAndRecv() (*DioMsg, error)
	grpc.ClientStream
}

type fpgaMemSetMultiClient struct {
	grpc.ClientStream
}

func (x *fpgaMemSetMultiClient) Send(m *DioMsg) error {
	return x.ClientStream.SendMsg(m)
}

func (x *fpgaMemSetMultiClient) CloseAndRecv() (*DioMsg, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(DioMsg)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// FpgaMemServer is the server API for FpgaMem service.
// All implementations must embed UnimplementedFpgaMemServer
// for forward compatibility
type FpgaMemServer interface {
	Peek(context.Context, *AddrMsg) (*ValueMsg, error)
	Poke(context.Context, *ValueMsg) (*ValueMsg, error)
	Modify(context.Context, *UpdateMsg) (*ValueMsg, error)
	DioGet(context.Context, *DioMsg) (*DioMsg, error)
	DioSet(context.Context, *DioMsg) (*DioMsg, error)
	PeekMulti(FpgaMem_PeekMultiServer) error
	SetMulti(FpgaMem_SetMultiServer) error
	mustEmbedUnimplementedFpgaMemServer()
}

// UnimplementedFpgaMemServer must be embedded to have forward compatible implementations.
type UnimplementedFpgaMemServer struct {
}

func (UnimplementedFpgaMemServer) Peek(context.Context, *AddrMsg) (*ValueMsg, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Peek not implemented")
}
func (UnimplementedFpgaMemServer) Poke(context.Context, *ValueMsg) (*ValueMsg, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Poke not implemented")
}
func (UnimplementedFpgaMemServer) Modify(context.Context, *UpdateMsg) (*ValueMsg, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Modify not implemented")
}
func (UnimplementedFpgaMemServer) DioGet(context.Context, *DioMsg) (*DioMsg, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DioGet not implemented")
}
func (UnimplementedFpgaMemServer) DioSet(context.Context, *DioMsg) (*DioMsg, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DioSet not implemented")
}
func (UnimplementedFpgaMemServer) PeekMulti(FpgaMem_PeekMultiServer) error {
	return status.Errorf(codes.Unimplemented, "method PeekMulti not implemented")
}
func (UnimplementedFpgaMemServer) SetMulti(FpgaMem_SetMultiServer) error {
	return status.Errorf(codes.Unimplemented, "method SetMulti not implemented")
}
func (UnimplementedFpgaMemServer) mustEmbedUnimplementedFpgaMemServer() {}

// UnsafeFpgaMemServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to FpgaMemServer will
// result in compilation errors.
type UnsafeFpgaMemServer interface {
	mustEmbedUnimplementedFpgaMemServer()
}

func RegisterFpgaMemServer(s grpc.ServiceRegistrar, srv FpgaMemServer) {
	s.RegisterService(&FpgaMem_ServiceDesc, srv)
}

func _FpgaMem_Peek_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddrMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpgaMemServer).Peek(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.FpgaMem/Peek",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpgaMemServer).Peek(ctx, req.(*AddrMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _FpgaMem_Poke_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ValueMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpgaMemServer).Poke(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.FpgaMem/Poke",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpgaMemServer).Poke(ctx, req.(*ValueMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _FpgaMem_Modify_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpgaMemServer).Modify(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.FpgaMem/Modify",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpgaMemServer).Modify(ctx, req.(*UpdateMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _FpgaMem_DioGet_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DioMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpgaMemServer).DioGet(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.FpgaMem/DioGet",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpgaMemServer).DioGet(ctx, req.(*DioMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _FpgaMem_DioSet_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DioMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpgaMemServer).DioSet(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.FpgaMem/DioSet",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpgaMemServer).DioSet(ctx, req.(*DioMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _FpgaMem_PeekMulti_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(FpgaMemServer).PeekMulti(&fpgaMemPeekMultiServer{stream})
}

type FpgaMem_PeekMultiServer interface {
	Send(*ValueMsg) error
	Recv() (*AddrMsg, error)
	grpc.ServerStream
}

type fpgaMemPeekMultiServer struct {
	grpc.ServerStream
}

func (x *fpgaMemPeekMultiServer) Send(m *ValueMsg) error {
	return x.ServerStream.SendMsg(m)
}

func (x *fpgaMemPeekMultiServer) Recv() (*AddrMsg, error) {
	m := new(AddrMsg)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _FpgaMem_SetMulti_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(FpgaMemServer).SetMulti(&fpgaMemSetMultiServer{stream})
}

type FpgaMem_SetMultiServer interface {
	SendAndClose(*DioMsg) error
	Recv() (*DioMsg, error)
	grpc.ServerStream
}

type fpgaMemSetMultiServer struct {
	grpc.ServerStream
}

func (x *fpgaMemSetMultiServer) SendAndClose(m *DioMsg) error {
	return x.ServerStream.SendMsg(m)
}

func (x *fpgaMemSetMultiServer) Recv() (*DioMsg, error) {
	m := new(DioMsg)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// FpgaMem_ServiceDesc is the grpc.ServiceDesc for FpgaMem service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var FpgaMem_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "api.FpgaMem",
	HandlerType: (*FpgaMemServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Peek",
			Handler:    _FpgaMem_Peek_Handler,
		},
		{
			MethodName: "Poke",
			Handler:    _FpgaMem_Poke_Handler,
		},
		{
			MethodName: "Modify",
			Handler:    _FpgaMem_Modify_Handler,
		},
		{
			MethodName: "DioGet",
			Handler:    _FpgaMem_DioGet_Handler,
		},
		{
			MethodName: "DioSet",
			Handler:    _FpgaMem_DioSet_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "PeekMulti",
			Handler:       _FpgaMem_PeekMulti_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
		{
			StreamName:    "SetMulti",
			Handler:       _FpgaMem_SetMulti_Handler,
			ClientStreams: true,
		},
	},
	Metadata: "api.proto",
}
