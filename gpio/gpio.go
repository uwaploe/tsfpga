// Package gpio hooks an FpgaMemClient into the periph library
package gpio

import (
	"context"
	"errors"
	"fmt"
	"time"

	pb "bitbucket.org/uwaploe/tsfpga/api"
	"periph.io/x/conn/v3/gpio"
	"periph.io/x/conn/v3/gpio/gpioreg"
	"periph.io/x/conn/v3/physic"
	gpiopin "periph.io/x/conn/v3/pin"
)

type pin struct {
	client  pb.FpgaMemClient
	name    string
	channel int
}

var ts4200Pins = []string{
	"8160_LCD_D0",
	"8160_LCD_D1",
	"8160_LCD_D2",
	"8160_LCD_D3",
	"8160_LCD_D4",
	"8160_LCD_D5",
	"8160_LCD_D6",
	"8160_LCD_D7",
	"8160_DIO_1",
	"8160_DIO_3",
	"8160_DIO_5",
	"8160_DIO_7",
	"8160_DIO_9",
	"8160_DIO_11",
	"8160_DIO_13",
	"8160_DIO_15",
}

var ts4800Pins = []string{
	"8100_LCD_D0",
	"8100_LCD_D1",
	"130",
	"131",
	"132",
	"133",
}

func registerBoardPins(client pb.FpgaMemClient, names []string) error {
	for i, name := range names {
		p := &pin{
			client:  client,
			name:    name,
			channel: i,
		}
		if err := gpioreg.Register(p); err != nil {
			return err
		}
	}

	return nil
}

func RegisterPins(client pb.FpgaMemClient, board string) error {
	switch board {
	case "4200":
		return registerBoardPins(client, ts4200Pins)
	case "4800":
		return registerBoardPins(client, ts4800Pins)
	}
	return fmt.Errorf("Invalid board name: %s", board)
}

func UnregisterPins(board string) error {
	var names []string
	switch board {
	case "4200":
		names = ts4200Pins
	case "4800":
		names = ts4800Pins
	}

	for _, name := range names {
		if err := gpioreg.Unregister(name); err != nil {
			return err
		}
	}

	return fmt.Errorf("Invalid board name: %s", board)
}

func dioset(client pb.FpgaMemClient, name string, state pb.DioState) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{
		Name:  name,
		State: state}
	_, err := client.DioSet(ctx, msg)
	return err
}

func dioget(client pb.FpgaMemClient, name string) (pb.DioState, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{Name: name}
	resp, err := client.DioGet(ctx, msg)
	if err != nil {
		return pb.DioState_LOW, err
	}
	return resp.State, nil
}

func (p *pin) Name() string {
	return p.name
}

func (p *pin) Number() int {
	return p.channel
}

func (p *pin) Out(l gpio.Level) error {
	var state pb.DioState
	switch l {
	case gpio.Low:
		state = pb.DioState_LOW
	case gpio.High:
		state = pb.DioState_HIGH
	}
	return dioset(p.client, p.name, state)
}

func (p *pin) In(_ gpio.Pull, _ gpio.Edge) error {
	return dioset(p.client, p.name, pb.DioState_INPUT)
}

func (p *pin) Read() gpio.Level {
	state, _ := dioget(p.client, p.name)
	switch state {
	case pb.DioState_HIGH, pb.DioState_INPUT_HIGH:
		return gpio.High
	}
	return gpio.Low
}

func (p *pin) WaitForEdge(timeout time.Duration) bool {
	return false
}

func (p *pin) Pull() gpio.Pull {
	return gpio.Float
}

func (p *pin) DefaultPull() gpio.Pull {
	return gpio.Float
}

func (p *pin) PWM(duty gpio.Duty, _ physic.Frequency) error {
	return errors.New("tsfpga: PWM not supported")
}

func (p *pin) SupportedFuncs() []gpiopin.Func {
	return []gpiopin.Func{gpio.IN, gpio.OUT}
}

func (p *pin) Func() gpiopin.Func {
	return gpio.OUT
}

func (p *pin) Function() string {
	return string(p.Func())
}

func (p *pin) Halt() error {
	return p.Out(gpio.Low)
}

func (p *pin) String() string {
	return p.Name()
}
