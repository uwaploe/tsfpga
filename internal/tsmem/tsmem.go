// Package tsmem provides access to a memory mapped segment of /dev/mem
package tsmem

import (
	"errors"
	"os"
	"runtime"
	"sync"
	"syscall"
	"unsafe"
)

var ErrBadAddress = errors.New("Invalid address")

type MemBlock struct {
	buf    []byte
	offset uint32
	mu     *sync.Mutex
}

func unmap(m *MemBlock) error {
	if m.buf == nil {
		return nil
	}
	mbuf := m.buf
	m.buf = nil
	runtime.SetFinalizer(m, nil)
	return syscall.Munmap(mbuf)
}

func New(base, size uint32) (*MemBlock, error) {
	f, err := os.OpenFile("/dev/mem", os.O_RDWR|os.O_SYNC, 0660)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// The mmap'ed buffer must start on a page boundary
	page := int64(base &^ 0xfff)
	m := &MemBlock{}
	m.offset = base - uint32(page)
	m.buf, err = syscall.Mmap(int(f.Fd()),
		page,
		int(m.offset+size),
		syscall.PROT_READ|syscall.PROT_WRITE,
		syscall.MAP_SHARED)
	if err != nil {
		return nil, err
	}
	m.mu = &sync.Mutex{}
	runtime.SetFinalizer(m, unmap)

	return m, nil
}

func (m *MemBlock) CheckAddr(addr uint32) error {
	if int(addr+m.offset) >= cap(m.buf) {
		return ErrBadAddress
	}
	return nil
}

func (m *MemBlock) ReadByte(addr uint32) uint8 {
	p := (*uint8)(unsafe.Pointer(&m.buf[addr+m.offset]))
	return *p
}

func (m *MemBlock) ReadWord(addr uint32) uint16 {
	p := (*uint16)(unsafe.Pointer(&m.buf[addr+m.offset]))
	return *p
}

func (m *MemBlock) ReadLong(addr uint32) uint32 {
	p := (*uint32)(unsafe.Pointer(&m.buf[addr+m.offset]))
	return *p
}

func (m *MemBlock) WriteByte(addr uint32, value uint8) uint8 {
	p := (*uint8)(unsafe.Pointer(&m.buf[addr+m.offset]))
	old := *p
	*p = value
	return old
}

func (m *MemBlock) WriteWord(addr uint32, value uint16) uint16 {
	p := (*uint16)(unsafe.Pointer(&m.buf[addr+m.offset]))
	old := *p
	*p = value
	return old
}

func (m *MemBlock) WriteLong(addr uint32, value uint32) uint32 {
	p := (*uint32)(unsafe.Pointer(&m.buf[addr+m.offset]))
	old := *p
	*p = value
	return old
}

func (m *MemBlock) ModByte(addr uint32, value, mask uint8) uint8 {
	m.mu.Lock()
	p := (*uint8)(unsafe.Pointer(&m.buf[addr+m.offset]))
	old := *p
	*p = (old &^ mask) | (value & mask)
	m.mu.Unlock()
	return old
}

func (m *MemBlock) ModWord(addr uint32, value, mask uint16) uint16 {
	m.mu.Lock()
	p := (*uint16)(unsafe.Pointer(&m.buf[addr+m.offset]))
	old := *p
	*p = (old &^ mask) | (value & mask)
	m.mu.Unlock()
	return old
}

func (m *MemBlock) ModLong(addr uint32, value, mask uint32) uint32 {
	m.mu.Lock()
	p := (*uint32)(unsafe.Pointer(&m.buf[addr+m.offset]))
	old := *p
	*p = (old &^ mask) | (value & mask)
	m.mu.Unlock()
	return old
}
