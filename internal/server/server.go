// Package server provides the gRPC server interface
package server

import (
	"context"
	"fmt"
	"io"

	"bitbucket.org/uwaploe/tsfpga/api"
	pb "bitbucket.org/uwaploe/tsfpga/api"
)

type Memory interface {
	CheckAddr(uint32) error
	ReadByte(uint32) uint8
	ReadWord(uint32) uint16
	ReadLong(uint32) uint32
	WriteByte(uint32, uint8) uint8
	WriteWord(uint32, uint16) uint16
	WriteLong(uint32, uint32) uint32
	ModByte(uint32, uint8, uint8) uint8
	ModWord(uint32, uint16, uint16) uint16
	ModLong(uint32, uint32, uint32) uint32
}

type Dio struct {
	DirReg uint32
	OutReg uint32
	InReg  uint32
	Mask   uint16
}

func (d Dio) IsOutput(m Memory) bool {
	return (m.ReadWord(d.DirReg) & d.Mask) == d.Mask
}

func (d Dio) State(m Memory) pb.DioState {
	if d.IsOutput(m) {
		if m.ReadWord(d.OutReg)&d.Mask == d.Mask {
			return pb.DioState_HIGH
		}
		return pb.DioState_LOW
	}

	if m.ReadWord(d.InReg)&d.Mask == d.Mask {
		return pb.DioState_INPUT_HIGH
	}
	return pb.DioState_INPUT_LOW
}

func (d Dio) SetState(m Memory, state pb.DioState) {
	switch state {
	case pb.DioState_HIGH:
		m.ModWord(d.OutReg, d.Mask, d.Mask)
		m.ModWord(d.DirReg, d.Mask, d.Mask)
	case pb.DioState_LOW:
		m.ModWord(d.OutReg, 0, d.Mask)
		m.ModWord(d.DirReg, d.Mask, d.Mask)
	case pb.DioState_INPUT:
		m.ModWord(d.DirReg, 0, d.Mask)
	}
}

type Server struct {
	api.UnimplementedFpgaMemServer
	dioMap map[string]Dio
	mem    Memory
}

func New(dio map[string]Dio, mem Memory) *Server {
	s := &Server{
		mem: mem,
	}
	s.dioMap = make(map[string]Dio)
	for k, v := range dio {
		s.dioMap[k] = v
	}

	return s
}

func (s *Server) DioGet(ctx context.Context, msg *pb.DioMsg) (*pb.DioMsg, error) {
	dio, ok := s.dioMap[msg.Name]
	if !ok {
		return nil, fmt.Errorf("Invalid name: %q", msg.Name)
	}

	return &pb.DioMsg{
		Name:  msg.Name,
		State: dio.State(s.mem),
	}, nil
}

func (s *Server) DioSet(ctx context.Context, msg *pb.DioMsg) (*pb.DioMsg, error) {
	dio, ok := s.dioMap[msg.Name]
	if !ok {
		return nil, fmt.Errorf("Invalid name: %q", msg.Name)
	}
	dio.SetState(s.mem, msg.State)
	return &pb.DioMsg{
		Name:  msg.Name,
		State: dio.State(s.mem),
	}, nil
}

func (s *Server) SetMulti(stream pb.FpgaMem_SetMultiServer) error {
	lastMsg := &pb.DioMsg{}

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		if dio, ok := s.dioMap[msg.Name]; ok {
			dio.SetState(s.mem, msg.State)
		} else {
			return fmt.Errorf("Invalid name: %q", msg.Name)
		}
		lastMsg = msg
	}

	return stream.SendAndClose(lastMsg)
}

func (s *Server) Peek(ctx context.Context, msg *pb.AddrMsg) (*pb.ValueMsg, error) {
	if err := s.mem.CheckAddr(msg.Address); err != nil {
		return nil, err
	}

	vm := pb.ValueMsg{Address: msg.Address, Size: msg.Size}
	switch msg.Size {
	case pb.Size_BYTE:
		vm.Value = uint32(s.mem.ReadByte(msg.Address)) & 0xff
	case pb.Size_WORD:
		vm.Value = uint32(s.mem.ReadWord(msg.Address)) & 0xffff
	case pb.Size_LONG:
		vm.Value = uint32(s.mem.ReadLong(msg.Address))
	}

	return &vm, nil
}

func (s *Server) PeekMulti(stream pb.FpgaMem_PeekMultiServer) error {
	for {
		addr, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		vm, err := s.Peek(context.Background(), addr)
		if err != nil {
			return err
		}
		if err := stream.Send(vm); err != nil {
			return err
		}
	}
}

func (s *Server) Poke(ctx context.Context, msg *pb.ValueMsg) (*pb.ValueMsg, error) {
	if err := s.mem.CheckAddr(msg.Address); err != nil {
		return nil, err
	}

	vm := pb.ValueMsg{
		Address: msg.Address,
		Size:    msg.Size,
	}

	switch msg.Size {
	case pb.Size_BYTE:
		vm.Value = uint32(s.mem.WriteByte(msg.Address, uint8(msg.Value))) & 0xff
	case pb.Size_WORD:
		vm.Value = uint32(s.mem.WriteWord(msg.Address, uint16(msg.Value))) & 0xffff
	case pb.Size_LONG:
		vm.Value = uint32(s.mem.WriteLong(msg.Address, uint32(msg.Value)))
	}

	return &vm, nil
}

func (s *Server) Modify(ctx context.Context, msg *pb.UpdateMsg) (*pb.ValueMsg, error) {
	if err := s.mem.CheckAddr(msg.Address); err != nil {
		return nil, err
	}

	vm := pb.ValueMsg{Address: msg.Address, Size: msg.Size}
	switch msg.Size {
	case pb.Size_BYTE:
		vm.Value = uint32(s.mem.ModByte(msg.Address, uint8(msg.Value), uint8(msg.Mask))) & 0xff
	case pb.Size_WORD:
		vm.Value = uint32(s.mem.ModWord(msg.Address, uint16(msg.Value), uint16(msg.Mask))) & 0xffff
	case pb.Size_LONG:
		vm.Value = uint32(s.mem.ModLong(msg.Address, uint32(msg.Value), uint32(msg.Mask)))
	}

	return &vm, nil
}
