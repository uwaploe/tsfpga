//
package adc

import (
	"errors"
	"fmt"

	"periph.io/x/conn/v3/analog"
	"periph.io/x/conn/v3/physic"
	"periph.io/x/conn/v3/pin"
)

type analogPin struct {
	adc  *TsAdc
	cnum uint
}

func (a *TsAdc) PinForChannel(n uint) (analog.PinADC, error) {
	if n >= ChanCount {
		return nil, fmt.Errorf("Invalid channel: %d", n)
	}

	return &analogPin{
		adc:  a,
		cnum: n,
	}, nil
}

func (p *analogPin) Range() (analog.Sample, analog.Sample) {
	min := analog.Sample{Raw: 0, V: 0 * physic.Volt}
	vmax := float64(p.adc.VoltsPerCount(p.cnum)) * float64(p.adc.cntRange) * 1e9
	max := analog.Sample{Raw: int32(p.adc.cntRange), V: physic.ElectricPotential(vmax)}
	return min, max
}

// Read returns the current pin level.
func (p *analogPin) Read() (analog.Sample, error) {
	s := analog.Sample{}
	val, err := p.adc.ReadCounts(p.cnum)
	if err != nil {
		return s, err
	}
	s.Raw = int32(val)
	s.V = physic.ElectricPotential(float64(p.adc.VoltsPerCount(p.cnum)) * float64(val) * 1e9)
	return s, nil
}

func (p *analogPin) Name() string {
	return fmt.Sprintf("TsADC(%d)", p.cnum)
}

func (p *analogPin) Number() int {
	return int(p.cnum)
}

func (p *analogPin) Function() string {
	return string(p.Func())
}

// Func implements pin.PinFunc.
func (p *analogPin) Func() pin.Func {
	return analog.ADC
}

// SupportedFuncs implements pin.PinFunc.
func (p *analogPin) SupportedFuncs() []pin.Func {
	return []pin.Func{analog.ADC}
}

// SetFunc implements pin.PinFunc.
func (p *analogPin) SetFunc(f pin.Func) error {
	if f == analog.ADC {
		return nil
	}
	return errors.New("pin function cannot be changed")
}

func (p *analogPin) Halt() error {
	return nil
}

func (p *analogPin) String() string {
	return p.Name()
}
