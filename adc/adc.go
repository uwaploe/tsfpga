// Package adc provides an interface to the analog to digital converter accessed
// through the FPGA on the TS-4800 and TS-4200 CPU boards.
package adc

import (
	"context"
	"errors"
	"fmt"
	"time"

	pb "bitbucket.org/uwaploe/tsfpga/api"
)

// Register offsets
const (
	RegCfg  uint32 = 0
	RegMask        = 0x02
	RegData        = 0x04
)

type Gain uint32

const (
	GainX1 Gain = 0
	GainX2      = 1
	GainX4      = 2
	GainX8      = 3
)

type Size uint32

const (
	Size12Bits Size = 0
	Size14Bits      = (1 << 2)
	Size16Bits      = (2 << 2)
)

type AnSel uint32

const (
	AnSelNone  AnSel = 0
	AnSelPin77       = (1 << 4)
	AnSelPin74       = (2 << 4)
)

const ChanCount = 6
const AllChans uint32 = (1 << ChanCount) - 1

var ErrChannel = errors.New("Invalid channel")
var ErrBoard = errors.New("Unsupported CPU board")

type Adc interface {
	ReadCounts(n uint) (int, error)
	VoltsPerCount(n uint) float32
}

type TsAdc struct {
	client   pb.FpgaMemClient
	chans    int
	baseAddr uint32
	cntRange int
	gain     float32
}

func New(client pb.FpgaMemClient) (*TsAdc, error) {
	var base uint

	resp, _ := peek(client, &pb.AddrMsg{Address: 0, Size: pb.Size_WORD})
	switch resp.Value {
	case 0x4200:
		base = 0x80
	case 0x4800:
		base = 0x6000
	default:
		return nil, fmt.Errorf("%#x: %w", resp.Value, ErrBoard)
	}

	return &TsAdc{
		client:   client,
		chans:    6,
		baseAddr: uint32(base),
	}, nil
}

func poke(client pb.FpgaMemClient, msg *pb.ValueMsg) (*pb.ValueMsg, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	return client.Poke(ctx, msg)
}

func peek(client pb.FpgaMemClient, msg *pb.AddrMsg) (*pb.ValueMsg, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	return client.Peek(ctx, msg)
}

func (a *TsAdc) Configure(bits Size, gain Gain, ansel AnSel) error {
	vm := &pb.ValueMsg{
		Address: a.baseAddr + RegCfg,
		Size:    pb.Size_WORD,
		Value:   uint32(bits) | uint32(gain) | uint32(ansel),
	}
	_, err := poke(a.client, vm)
	if err != nil {
		return fmt.Errorf("Cannot set cfg register (%x): %w", vm.Value, err)
	}

	switch gain {
	case GainX1:
		a.gain = 1
	case GainX2:
		a.gain = 2
	case GainX4:
		a.gain = 4
	case GainX8:
		a.gain = 8
	}

	switch bits {
	case Size12Bits:
		a.cntRange = 1 << 11
	case Size14Bits:
		a.cntRange = 1 << 13
	case Size16Bits:
		a.cntRange = 1 << 15
	}

	vm = &pb.ValueMsg{
		Address: a.baseAddr + RegMask,
		Size:    pb.Size_WORD,
		Value:   AllChans,
	}
	_, err = poke(a.client, vm)

	return err
}

func (a *TsAdc) VoltsPerCount(n uint) float32 {
	if n >= ChanCount {
		return 0
	}

	volts := []float32{2.048, 2.048, 10.24, 10.24, 10.24, 10.24}
	rngMult := []float32{2, 2, 1, 1, 1, 1}
	return volts[int(n)] / (float32(a.cntRange) * rngMult[int(n)] * a.gain)
}

func (a *TsAdc) ReadCounts(n uint) (int, error) {
	if n >= ChanCount {
		return 0, fmt.Errorf("%d: %w", n, ErrChannel)
	}

	msg := &pb.AddrMsg{
		Address: a.baseAddr + RegData + (uint32(n) * 2),
		Size:    pb.Size_WORD,
	}
	resp, err := peek(a.client, msg)

	return int(resp.Value), err
}
